# Autor: Jan Herec
# Popis: ukol do predmetu PBI na FIT VUT v Brne
# Zadani: Vytvorte skript, ktery pro aktualne zobrazeny protein vypocita stred proteinu

from __future__ import print_function
from pymol import *

# funkce na spocitani stredu aktualniho proteinu
all_atoms = []
def computeMass():
	# ulozime si vsechny atomy do promene all_atoms
	cmd.iterate_state(-1, "all", 'all_atoms.append([x, y, z])')

	# sumy suřadnic atomů v různých dimenzích
	x_position_sum = 0.0
	y_position_sum = 0.0
	z_position_sum = 0.0

	# spocitame sumy suřadnic atomů v různých dimenzích, tyto následně podělíme počtem atomů a získáme aritmetické průměry pro pozice v daných dimenzích a celkově tak doataneme stred hmoty
	for atom in all_atoms:
		x_position_sum += atom[0]
		y_position_sum += atom[1]
		z_position_sum += atom[2]
	
	print("Center of protein: [%s, %s, %s]" % (x_position_sum / len(all_atoms), y_position_sum / len(all_atoms), z_position_sum / len(all_atoms)))

	
# Prikaz centerofmass je dostupny od verze PyMOL 1.7.2, viz https://pymolwiki.org/index.php/Centerofmass
try:
	print("Center of protein: %s" % (str(cmd.centerofmass())) )
# pokud se pouzije verze PyMOL mensi jak 1.7.2, potom je treba spocitat centr atomu rucne strucne, takto spocteny centr se muze lisit od centru spocitaneho prikazem centerofmass, ktery se nejspise pocita nejak sofistikovaneji
except:
	computeMass()

