# Autor: Jan Herec
# Popis: ukol do predmetu PBI na FIT VUT v Brne
# Zadani: Vytvorte skript, ktery najde v proteinu par atomu s minimalni (krome sousednich v primarni strukture) a maximalni vzdialenosti a vyznaci je v zobrazeni barvou a vzdalenosti.

from pymol import *
import sys
import math

# pripravime si kreslici plochu, jako protein na kterem budeme ukazovat funkcnost algoritmu si zvolime protein 3NIR
cmd.delete("all")
cmd.fetch("3NIR", async=0)
cmd.hide("all")
cmd.show("lines")
cmd.color("blue", "all")

# ulozime si vsechny atomy do promene all_atoms
all_atoms = []
cmd.iterate_state(-1, "all", 'all_atoms.append([x, y, z, index])')

# pro kazdy atom si ulozime jeho sousedni atomy 
atoms_neighbors = []
for atom in all_atoms:
	atom_neighbors = []
	cmd.iterate("neighbor index %s" % (str(atom[3])), "atom_neighbors.append(index)")
	atoms_neighbors.append(atom_neighbors)
	# overeni ze toto funguje
	#if (len(atom_neighbors) == 0):
	#	cmd.select("sel", "index %s" % (str(atom[3])))
	#	cmd.color("green", "sel")

	
# indexy atomu s minimalni a maximalni vzdalenosti	
min_atom_indexes = []
max_atom_indexes = []
# minimalni a maximalni vzdalenosti
min_distance = sys.float_info.max
max_distance = sys.float_info.min

# nalezenem dvojice s minimalni a maximalni vzdalenosti
for index1, atom1 in enumerate(all_atoms):
	# jsme na konci kontroly dvojic, koncime cyklus
	if index1 + 1 == len(all_atoms):
		break
	# prohledavame dvojice ktere jsme zatim nezkouseli
 	for atom2 in all_atoms[(index1+1):]:
		# pokud nejsou atomy v prime vazbe, ma cenu zkusit vypocitat jejich euklidovskou vzdalenost
		if (atom2[3] not in atoms_neighbors[index1]):
			distance = math.sqrt((atom1[0] - atom2[0])**2 + (atom1[1] - atom2[1])**2 + (atom1[2] - atom2[2])**2)
			# pokud je vzdalenost vetsi nez doposud nejvetsi nalezena, schovame si indexy atomu takto maximalne vzdalenych
			if (distance > max_distance):
				max_distance = distance
				max_atom_indexes = [atom1[3], atom2[3]]
			# pokud je vzdalenost mensi nez doposud nejmensi nalezena, schovame si indexy atomu takto minimalne vzdalenych
			if (distance < min_distance):
				min_distance = distance
				min_atom_indexes = [atom1[3], atom2[3]]

# vybereme pary atomu s minimalni a maximalni vzdalenosti podle jejich indexu	
cmd.select("atom_min_distance", "index %s or index %s" % ( str(min_atom_indexes[0]), str(min_atom_indexes[1]) ))
cmd.select("atom_max_distance", "index %s or index %s" % ( str(max_atom_indexes[0]), str(max_atom_indexes[1]) ))

# obarvime nejvzdalenejsi a nejblizsi atomove pary a vyznacime je
cmd.color("red", "atom_min_distance")
cmd.color("yellow", "atom_max_distance")
cmd.set("sphere_scale", 0.05, "all")
cmd.set("sphere_transparency", 0.3, "all")

cmd.show('sphere', "all")

# zobrazime vzdalenosti mezi nejblizsimi a nejvzdalenejsimi atomovymi pary
cmd.distance("distance_object_min", "atom_min_distance", "atom_min_distance")
cmd.distance("distance_object_max", "atom_max_distance", "atom_max_distance")
cmd.color("red", "distance_object_min")
cmd.color("yellow", "distance_object_max")

