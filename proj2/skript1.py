# Autor: Jan Herec
# Popis: ukol do predmetu PBI na FIT VUT v Brne
# Zadani: Napiste skript pro Pymol, ktery v libovolne strukture identifikuje cysteiny, obarvi je na zluto, spocita jejich vzdalenost a ty, ktere by mohly tvorit cysteinove mostiky, obarvi oranzovou a vypise ve strukture jejich identifikator a delku vazby

from pymol import *
import sys
import math

# pripravime si kreslici plochu, jako protein na kterem budeme ukazovat funkcnost algoritmu si zvolime protein 1YVB
cmd.delete("all")
cmd.fetch("1YVB", async=0)
cmd.hide("all")
cmd.show("lines")
cmd.color("blue", "all")

# vybereme vsechny cysteiny a obarvime je na zlato
cmd.select("allcysteine", "resn cys")
cmd.show("spheres", "allcysteine")
cmd.set("sphere_scale", 0.2, "allcysteine")
cmd.color("yellow", "allcysteine")

# chceme najit disulfidicke mustky, ktere tvori cysteiny, tedy vybereme vsechny atomy siry v ramci cysteinu a uozime je do promenne all_cysteine_sulfid_atoms
all_cysteine_sulfid_atoms = []
cmd.iterate_state(-1, "cys/sg", 'all_cysteine_sulfid_atoms.append([x, y, z, index])')

# indexy atomu siry, ktere tvori disulfidicke mustky  
sulfid_bridge_atoms_indexes = set()

# nalezenem atomu siry, ktere tvori disulfidicke mustky (vzdalenost atomy siry mensi jak 2.5 A)
for index1, atom1 in enumerate(all_cysteine_sulfid_atoms):
	# jsme na konci kontroly disulfidickych mustku, koncime cyklus
	if index1 + 1 == len(all_cysteine_sulfid_atoms):
		break
		
	# prohledavame dvojice atomu siry, ktere jsme zatim nezkouseli
 	for atom2 in all_cysteine_sulfid_atoms[(index1+1):]:
		# vypocitame euklidovskou vzdalenos mezi atomy siry
		distance = math.sqrt((atom1[0] - atom2[0])**2 + (atom1[1] - atom2[1])**2 + (atom1[2] - atom2[2])**2)
		# pokud je vzdalenost mensi jak jak 2.5 A, potom je pravdepodobne ze dane siry tvori sulfidovy mustek a ulozime si je
		if (distance < 2.5):
			sulfid_bridge_atoms_indexes.update([atom1[3], atom2[3]])
			

# sestavime select dotaz podle vybranych atomu siry
select_query = ""			
for atom_index in sulfid_bridge_atoms_indexes:
	if (len(select_query) == 0):
		select_query += "index %s" % (str(atom_index))
	else:
		select_query += "or index %s" % (str(atom_index))
		
		
# vybereme atomu siry, ktere tvori disulfidicke mustky podle jejich indexu	
cmd.select("sulfid_bridge_atoms", select_query)
# vybereme residua (cysteiny) podle vybranych atomu siry
cmd.select("allcysteine_with_sulfid_bridge", "byresidue sulfid_bridge_atoms")
# dane vybrane residua (cysteiny) obarvime na oranzovo
cmd.color("orange", "allcysteine_with_sulfid_bridge")
# oznacime atomy siry tvorici cysteinove mustky identifikatorem jejich residuií (cysteiny) 
cmd.label("sulfid_bridge_atoms", "resi")

# nakonec si nechame vypsat vzdalensoti mezi vazbami atomu siry v disulfidickych mustcich
cmd.distance(None, "sulfid_bridge_atoms", "sulfid_bridge_atoms", cutoff=2.5, mode=0)


